<?php
/**
 * RequestFactoryTest
 *
 * @license MIT
 * @copyright 2018 Tommy Teasdale
 */

/** @noinspection PhpUnusedLocalVariableInspection */
/** @noinspection ReturnTypeCanBeDeclaredInspection */
/** @noinspection UnnecessaryAssertionInspection */
/** @noinspection PropertyCanBeStaticInspection */

declare(strict_types=1);

use Apine\Http\Factories\RequestFactory;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;

class RequestFactoryTest extends TestCase
{
    /**
     * @var RequestFactory
     */
    private $factory;
    
    private $server = [
        'REQUEST_METHOD' => 'GET',
        'SERVER_PROTOCOL' => 'HTTP/1.1',
        'HTTP_HOST' => 'google.com',
    ];
    
    public function setUp()
    {
        $this->factory = new RequestFactory();
    }
    
    public function testCreateServerRequest()
    {
        $request = $this->factory->createServerRequest('GET', 'https://google.com');
        $this->assertInstanceOf(ServerRequestInterface::class, $request);
    }
    
    public function testCreateServerRequestWithServerParams()
    {
        $request = $this->factory->createServerRequest('GET', 'https://google.com', $this->server);
        $this->assertInstanceOf(ServerRequestInterface::class, $request);
    }
    
    public function testCreateServerRequestFromGlobals()
    {
        $request = $this->factory->createServerRequestFromGlobals(
            $this->server
        );
        $this->assertInstanceOf(ServerRequestInterface::class, $request);
    }
}
