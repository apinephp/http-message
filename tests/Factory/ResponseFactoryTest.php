<?php
/**
 * ResponseFactoryTest
 *
 * @license MIT
 * @copyright 2018 Tommy Teasdale
 */

/** @noinspection ReturnTypeCanBeDeclaredInspection */
/** @noinspection UnnecessaryAssertionInspection */

declare(strict_types=1);

use Apine\Http\Factories\ResponseFactory;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

class ResponseFactoryTest extends TestCase
{
    /**
     * @var ResponseFactory
     */
    private $factory;
    
    public function setUp()
    {
        $this->factory = new ResponseFactory();
    }
    
    public function testCreateResponse()
    {
        $response = $this->factory->createResponse(404);
        $this->assertInstanceOf(ResponseInterface::class, $response);
    }
}
