<?php
/**
 * HeadersTest
 *
 * @license MIT
 * @copyright 2018 Tommy Teasdale
 */

/** @noinspection ReturnTypeCanBeDeclaredInspection */

declare(strict_types=1);

use Apine\Http\Collection;
use PHPUnit\Framework\TestCase;

class CollectionTest extends TestCase
{
    
    public function testReplace()
    {
        $array = [
            'foo' => 'bar',
            'foobar' => 'baz'
        ];
        $collection = $this->getMockForAbstractClass(Collection::class);
        
        $this->assertAttributeEmpty('array', $collection);
        
        $collection->replace($array);
        
        $this->assertAttributeNotEmpty('array', $collection);
        $this->assertAttributeEquals($array, 'array', $collection);
    }
    
    public function testAll()
    {
        $array = [
            'foo' => 'bar',
            'foobar' => 'baz'
        ];
        $collection = $this->getMockForAbstractClass(Collection::class);
        $collection->replace($array);
        
        $this->assertEquals($array, $collection->all());
    }
    
    /**
     * @depends testReplace
     */
    public function testHas()
    {
        $array = [
            'foo' => 'bar',
            'foobar' => 'baz'
        ];
        $collection = $this->getMockForAbstractClass(Collection::class);
        $collection->replace($array);
        
        $this->assertTrue($collection->has('foo'));
        $this->assertFalse($collection->has('bar'));
    }
    
    public function testSet()
    {
        $collection = $this->getMockForAbstractClass(Collection::class);
        $collection->set('bar', 'foo');
        $this->assertTrue($collection->has('bar'));
        $this->assertAttributeEquals(['bar'=>'foo'], 'array', $collection);
    }
    
    /**
     * @depends testReplace
     */
    public function testEmpty()
    {
        $array = [
            'foo' => 'bar',
            'foobar' => 'baz'
        ];
        $collection = $this->getMockForAbstractClass(Collection::class);
        $collection->replace($array);
        
        $collection->empty();
        $this->assertAttributeEmpty('array', $collection);
    }
    
    /**
     * @depends testReplace
     */
    public function testGet()
    {
        $array = [
            'foo' => 'bar',
            'foobar' => 'baz'
        ];
        $collection = $this->getMockForAbstractClass(Collection::class);
        $collection->replace($array);
        
        $value = $collection->get('foo');
        $this->assertEquals('bar', $value);
        $value = $collection->get('baz');
        $this->assertNull($value);
    }
    
    /**
     * @depends testReplace
     */
    public function testGetCustomDefaultValue()
    {
        $array = [
            'foo' => 'bar',
            'foobar' => 'baz'
        ];
        $collection = $this->getMockForAbstractClass(Collection::class);
        $collection->replace($array);
        
        $value = $collection->get('baz', 404);
        $this->assertEquals(404, $value);
    }
    
    /**
     * @depends testReplace
     * @depends testAll
     */
    public function testRemove()
    {
        $array = [
            'foo' => 'bar',
            'foobar' => 'baz'
        ];
        $collection = $this->getMockForAbstractClass(Collection::class);
        $collection->replace($array);
        
        $collection->remove('foobar');
        $array = $collection->all();
        
        $this->assertArrayNotHasKey('foobar', $array);
    }
}
