<?php
/**
 * RequestFactory
 *
 * @license MIT
 * @copyright 2018 Tommy Teasdale
 */
declare(strict_types=1);

namespace Apine\Http\Factories;

use Apine\Http\Request;
use Psr\Http\Message\ServerRequestFactoryInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;

/**
 * Class RequestFactory
 *
 * @package Apine\Http\Factories
 */
class RequestFactory implements ServerRequestFactoryInterface
{
    /**
     * @var UploadedFileFactory
     */
    private UploadedFileFactory $fileFactory;
    
    /**
     * @var UriFactory
     */
    private UriFactory $uriFactory;
    
    public function __construct()
    {
        $this->uriFactory = new UriFactory();
        $this->fileFactory = new UploadedFileFactory();
    }
    
    /**
     * Create a new server request.
     *
     * @param string              $method
     * @param UriInterface|string $uri
     * @param array               $serverParams
     *
     * @return ServerRequestInterface
     */
    public function createServerRequest(string $method, $uri, array $serverParams = []): ServerRequestInterface
    {
        if (empty($serverParams)) {
            return new Request($method, $uri);
        }
    
        $protocol = isset($serverParams['SERVER_PROTOCOL']) ? str_replace('HTTP/', '', $serverParams['SERVER_PROTOCOL']) : '1.1';
    
        return new Request(
            $method,
            $uri,
            [],
            null,
            $protocol,
            $serverParams
        );
    }
    
    /**
     * Create a new server request representing the
     * incoming request using all the server variables
     *
     * @param array $server Typically $_SERVER
     * @param array $get Typically $_GET
     * @param array $post Typically $_POST
     * @param array $files Typically $_FILES
     * @param array $cookies Typically $_COOKIES
     * @param array $headers List of incoming request headers usually coming from getallheaders()
     * @param null  $body Body of the incoming request
     *
     * @return \Psr\Http\Message\ServerRequestInterface
     */
    public function createServerRequestFromGlobals(
        array $server,
        array $get = [],
        array $post = [],
        array $files = [],
        array $cookies = [],
        array $headers = [],
        $body = null
    ): ServerRequestInterface {
        $uri = $this->uriFactory->createUriFromArray($server);
        
        $method = $server['REQUEST_METHOD'] ?? 'GET';
        
        $protocol = isset($server['SERVER_PROTOCOL']) ? str_replace('HTTP/', '', $server['SERVER_PROTOCOL']) : '1.1';
        
        $uploadedFiles = $this->fileFactory->createUploadedFileFromArray($files);
        
        $request = new Request(
            $method,
            $uri,
            $headers,
            $body,
            $protocol,
            $_SERVER
        );
        
        return $request
            ->withCookieParams($cookies)
            ->withQueryParams($get)
            ->withParsedBody($post)
            ->withUploadedFiles($uploadedFiles);
    }
}