<?php
/**
 * UploadedFileFactory
 *
 * @license MIT
 * @copyright 2018 Tommy Teasdale
 */
declare(strict_types=1);

namespace Apine\Http\Factories;

use Apine\Http\UploadedFile;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UploadedFileFactoryInterface;
use Psr\Http\Message\UploadedFileInterface;
use function is_array, count;
use const UPLOAD_ERR_OK;

/**
 * Class UploadedFileFactory
 *
 * @package Apine\Http\Factories
 */
class UploadedFileFactory implements UploadedFileFactoryInterface
{
    /**
     * Create a new uploaded file.
     * If a string is used to create the file, a temporary resource will be
     * created with the content of the string.
     * If a size is not provided it will be determined by checking the size of
     * the file.
     *
     * @see http://php.net/manual/features.file-upload.post-method.php
     * @see http://php.net/manual/features.file-upload.errors.php
     *
     * @param StreamInterface $stream
     * @param int             $size in bytes
     * @param int             $error PHP file upload error
     * @param string          $clientFilename
     * @param string          $clientMediaType
     *
     * @return UploadedFileInterface
     */
    public function createUploadedFile(
        StreamInterface $stream,
        int $size = null,
        int $error = UPLOAD_ERR_OK,
        string $clientFilename = null,
        string $clientMediaType = null
    ): UploadedFileInterface {
        return new UploadedFile(
            $stream,
            $size,
            $error,
            $clientFilename,
            $clientMediaType
        );
    }
    
    /**
     * @param array $files Typically $_FILES
     *
     * @return UploadedFileInterface[]
     */
    public function createUploadedFileFromArray(array $files): array
    {
        $normalized = [];
        $uploadedFiles = [];
        $streamFactory = new StreamFactory();
        
        foreach ($files as $index => $info) {
            $input = [];
            foreach ($info as $key => $valueArray) {
                if (is_array($valueArray)) { // file input multiple
                    foreach ($valueArray as $i => $value) {
                        $input[$i][$key] = $value;
                    }
                } else { // single file input
                    $input[] = $info;
                    break;
                }
            }
            
            $normalized[] = $input;
        }
        
        if (count($normalized) > 0) {
            $normalized = array_merge(...$normalized);
        }
        
        foreach ($normalized as $info) {
            $uploadedFiles[] = $this->createUploadedFile(
                $streamFactory->createStreamFromFile($info['tmp_name']),
                (int)$info['size'],
                (int)$info['error'],
                $info['name'],
                $info['type']
            );
        }
        
        return $uploadedFiles;
    }
}