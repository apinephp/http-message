<?php
/**
 * ResponseFactory
 *
 * @license MIT
 * @copyright 2018 Tommy Teasdale
 */
declare(strict_types=1);

namespace Apine\Http\Factories;

use Apine\Http\Response;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class ResponseFactory
 *
 * @package Apine\Http\Factories
 */
class ResponseFactory implements ResponseFactoryInterface
{
    /**
     * Create a new response.
     *
     * @param int    $code HTTP status code
     * @param string $reasonPhrase
     *
     * @return ResponseInterface
     */
    public function createResponse($code = 200, string $reasonPhrase = ''): ResponseInterface
    {
        return new Response(
            $code,
            [],
            null,
            '1.1',
            $reasonPhrase
        );
    }
}