<?php
/**
 * Collection
 *
 * @license MIT
 * @copyright 2018 Tommy Teasdale
 */
declare(strict_types=1);

namespace Apine\Http;

/**
 * Class Collection
 *
 * @package Apine\Http
 */
abstract class Collection implements CollectionInterface
{
    /**
     * @var array
     */
    private array $array = [];
    
    public function all() : array
    {
        return $this->array;
    }
    
    public function set(string $key, $value): void
    {
        $this->array[$key] = $value;
    }
    
    public function get(string $key, $default = null)
    {
        return $this->has($key) ? $this->array[$key] : $default;
    }
    
    public function has(string $key): bool
    {
        return isset($this->array[$key]);
    }
    
    public function remove(string $key): void
    {
        unset($this->array[$key]);
    }
    
    public function replace(array $array): void
    {
        $this->array = $array;
    }
    
    public function empty(): void
    {
        $this->array = [];
    }
}