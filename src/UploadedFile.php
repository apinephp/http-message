<?php
/**
 * UploadedFile
 *
 * @license MIT
 * @copyright 2018-2019 Tommy Teasdale
 */

declare(strict_types=1);

namespace Apine\Http;

use InvalidArgumentException;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UploadedFileInterface;

use RuntimeException;
use Throwable;
use const \UPLOAD_ERR_OK;
use function is_string, is_resource;

class UploadedFile implements UploadedFileInterface
{
    /**
     * @var StreamInterface
     */
    private StreamInterface $stream;
    
    /**
     * @var string
     */
    private string $file;
    
    /**
     * @var string
     */
    private ?string $clientMediaType = null;
    
    /**
     * @var string|null
     */
    private ?string $clientFilename = null;
    
    /**
     * @var int
     */
    private int $error;
    
    /**
     * @var bool
     */
    private bool $moved = false;
    
    /**
     * @var int
     */
    private int $size = 0;
    
    /**
     * UploadedFile constructor.
     *
     * @param resource|string|StreamInterface     $resource
     * @param int  $size
     * @param int  $errorStatus
     * @param string $clientFilename
     * @param string $clientMediaType
     */
    public function __construct(
        $resource,
        int $size = 0,
        int $errorStatus = UPLOAD_ERR_OK,
        $clientFilename = null,
        $clientMediaType = null
    ) {
        $this->size = $size;
        $this->error = $errorStatus;
        
        $this->setClientFilename($clientFilename);
        $this->setClientMediaType($clientMediaType);
        $this->setStream($resource);
    }
    
    private function setClientFilename(?string $filename): void
    {
        $this->clientFilename = $filename;
    }
    
    private function setClientMediaType(?string $mediaType): void
    {
        $this->clientMediaType = $mediaType;
    }
    
    private function isValid(): bool
    {
        return ($this->error === UPLOAD_ERR_OK);
    }
    
    private function isMoved(): bool
    {
        return ($this->moved === true);
    }
    
    /**
     * Retrieve a stream representing the uploaded file.
     * This method MUST return a StreamInterface instance, representing the
     * uploaded file. The purpose of this method is to allow utilizing native PHP
     * stream functionality to manipulate the file upload, such as
     * stream_copy_to_stream() (though the result will need to be decorated in a
     * native PHP stream wrapper to work with such functions).
     * If the moveTo() method has been called previously, this method MUST raise
     * an exception.
     *
     * @return StreamInterface Stream representation of the uploaded file.
     * @throws \RuntimeException in cases when no stream is available or can be
     *     created.
     */
    public function getStream(): StreamInterface
    {
        if (!$this->isValid()) {
            throw new RuntimeException('Cannot retrieve stream due to upload error');
        }
        
        if ($this->isMoved()) {
            throw new RuntimeException('Cannot retrieve stream after it has been moved');
        }
        
        return $this->stream;
    }
    
    /**
     * Move the uploaded file to a new location.
     * Use this method as an alternative to move_uploaded_file(). This method is
     * guaranteed to work in both SAPI and non-SAPI environments.
     * Implementations must determine which environment they are in, and use the
     * appropriate method (move_uploaded_file(), rename(), or a stream
     * operation) to perform the operation.
     * $targetPath may be an absolute path, or a relative path. If it is a
     * relative path, resolution should be the same as used by PHP's rename()
     * function.
     * The original file or stream MUST be removed on completion.
     * If this method is called more than once, any subsequent calls MUST raise
     * an exception.
     * When used in an SAPI environment where $_FILES is populated, when writing
     * files via moveTo(), is_uploaded_file() and move_uploaded_file() SHOULD be
     * used to ensure permissions and upload status are verified correctly.
     * If you wish to move to a stream, use getStream(), as SAPI operations
     * cannot guarantee writing to stream destinations.
     *
     * @see http://php.net/is_uploaded_file
     * @see http://php.net/move_uploaded_file
     *
     * @param string $targetPath Path to which to move the uploaded file.
     *
     * @throws \InvalidArgumentException if the $targetPath specified is invalid.
     * @throws \RuntimeException on any error during the move operation, or on
     *     the second or subsequent call to the method.
     */
    public function moveTo($targetPath): void
    {
        if ($this->isMoved()) {
            throw new RuntimeException('File has already been moved once');
        }
        
        if (!is_string($targetPath)) {
            throw new InvalidArgumentException('The specified path is invalid');
        }
        
        try {
            if (isset($this->file)) {
                if (is_uploaded_file($this->file)) {
                    move_uploaded_file($this->file, $targetPath);
                } else {
                    rename($this->file, $targetPath);
                }
            } else if ($this->stream) {
                $newfile = new Stream(fopen($targetPath, 'wb'));
    
                while (!$this->stream->eof()) {
                    if (!$newfile->write($this->stream->read(8192))) {
                        break;
                    }
                }
            }
        } catch (Throwable $e) {
            throw new RuntimeException(sprintf('File %1s could not be moved to %2s', $this->file ?? '', $targetPath));
        }
        
        $this->moved = true;
    }
    
    /**
     * Retrieve the file size.
     * Implementations SHOULD return the value stored in the "size" key of
     * the file in the $_FILES array if available, as PHP calculates this based
     * on the actual size transmitted.
     *
     * @return int|null The file size in bytes or null if unknown.
     */
    public function getSize(): ?int
    {
        return $this->size;
    }
    
    /**
     * Retrieve the error associated with the uploaded file.
     * The return value MUST be one of PHP's UPLOAD_ERR_XXX constants.
     * If the file was uploaded successfully, this method MUST return
     * UPLOAD_ERR_OK.
     * Implementations SHOULD return the value stored in the "error" key of
     * the file in the $_FILES array.
     *
     * @see http://php.net/manual/en/features.file-upload.errors.php
     * @return int One of the UPLOAD_ERR_XXX constants from PHP.
     */
    public function getError(): int
    {
        return $this->error;
    }
    
    /**
     * Retrieve the filename sent by the client.
     * Do not trust the value returned by this method. A client could send
     * a malicious filename with the intention to corrupt or hack your
     * application.
     * Implementations SHOULD return the value stored in the "name" key of
     * the file in the $_FILES array.
     *
     * @return string|null The filename sent by the client or null if none
     *     was provided.
     */
    public function getClientFilename(): ?string
    {
        return $this->clientFilename;
    }
    
    /**
     * Retrieve the media type sent by the client.
     * Do not trust the value returned by this method. A client could send
     * a malicious media type with the intention to corrupt or hack your
     * application.
     * Implementations SHOULD return the value stored in the "type" key of
     * the file in the $_FILES array.
     *
     * @return string|null The media type sent by the client or null if none
     *     was provided.
     */
    public function getClientMediaType(): ?string
    {
        return $this->clientMediaType;
    }
    
    /**
     * @param string|resource|StreamInterface $resource
     */
    private function setStream($resource): void
    {
        if (is_string($resource)) {
            $this->file = $resource;
    
            if (file_exists($resource)) {
                $this->stream = new Stream(fopen($resource, 'r+b'));
            }
        } else if (is_resource($resource)) {
            $this->stream = new Stream($resource);
        } else if ($resource instanceof StreamInterface) {
            $this->stream = $resource;
        } else {
            throw new InvalidArgumentException('Invalid resource provided');
        }
    }
}