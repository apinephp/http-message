<?php
/**
 * CollectionInterface
 *
 * @license MIT
 * @copyright 2018 Tommy Teasdale
 */
declare(strict_types=1);


namespace Apine\Http;


/**
 * Interface CollectionInterface
 *
 * @package Apine\Http
 */
interface CollectionInterface
{
    public function all();
    
    /**
     * Add an index in the collection and
     * assign a value at the index
     *
     * @param string $key
     * @param mixed  $value
     */
    public function set(string $key, $value);
    
    /**
     * Fetch the value at the index in the collection.
     * Returns a default value if the index is not in
     * the collection
     *
     * @param string $key
     * @param mixed  $default
     *
     * @return mixed
     */
    public function get(string $key, $default = null);
    
    /**
     * Check if the collection has index
     *
     * @param string $key
     *
     */
    public function has(string $key);
    
    /**
     * Remove an index from the collection
     *
     * @param string $key
     */
    public function remove(string $key);
    
    /**
     * Replace the content of the collection
     * with a new collection array
     *
     * @param array $array
     */
    public function replace(array $array);
    
    /**
     * Clear the content of the collection
     */
    public function empty();
}